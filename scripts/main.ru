require 'json'
require 'semver_dialects'
require 'pry'

class App
  def call(env)
    req = Rack::Request.new(env)
    case req.path_info
    when /health/
      [200, {"Content-Type" => "text/html"}, ["OK"]]
    else
      result = SemverDialects::VersionChecker.version_translate(req.params["type"], req.params["version"])

      [200, {"Content-Type" => "text/html"}, result]
    end
  end
end

run App.new
