package interop

import (
	"io"
	"log"
	"net/http"
	"os/exec"
	"time"
)

func Exec() {
	cmd := exec.Command("rackup", "--port", "8080", "--host", "0.0.0.0", "main.ru")
	cmd.Dir = "./scripts"

	if err := cmd.Start(); err != nil {
		log.Fatalf("error starting script: %s, %s", cmd.Path, err.Error())
	}

	go func() {
		for {
			resp, err := http.Get("http://0.0.0.0:8080?type=maven&version=8.8.8")
			if err != nil {
				log.Fatalln(err)
			}

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				log.Fatalln(err)
			}

			log.Printf("normalized version: %s", string(body))

			time.Sleep(5 * time.Second)
		}
	}()

	if err := cmd.Wait(); err != nil {
		log.Fatalf("error waiting for script: %s, %s", cmd.Path, err.Error())
	}
}
