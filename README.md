# Manual QA - Calling a Ruby Gem from Go

This test project demonstrates communicating with a Ruby Gem over `http` using [`rack`](https://github.com/rack/rack).

## Example

```bash
$ go run cmd/main.go
2023/09/19 23:44:03 normalized version: >=8.8.8
2023/09/19 23:44:08 normalized version: >=8.8.8
```
